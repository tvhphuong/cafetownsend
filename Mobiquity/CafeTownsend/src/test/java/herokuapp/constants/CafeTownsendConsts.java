package herokuapp.constants;

/**
 * Constants for Test Cafe TownSend Web Application
 *
 *  @author Phuong Trinh Vo Hoang
 */
public final class CafeTownsendConsts {
    public static final String DEFAULT_URL = "http://cafetownsend-angular-rails.herokuapp.com";
    public static final String GREETING_FORMAT = "Hello ";
    public static final String VALID_USERNAME = "Luke";
    public static final String VALID_PASSWORD = "Skywalker";
    public static final String LOGIN_ERROR_MESSAGE = "Invalid username or password!";
    public static final int LOADING_RETRY_POLLING = 1000;
    public static final int LOADING_RETRY_TIME = 10;
}