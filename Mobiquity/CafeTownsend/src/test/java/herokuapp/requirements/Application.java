package herokuapp.requirements;


import net.thucydides.core.annotations.Feature;

/**
 * Feature Application
 *
 *  @author Phuong Trinh Vo Hoang
 */
public class Application {
    @Feature
    public class Authentication {
    }

    @Feature
    public class CreateAndModifyEmployee {
    }
}