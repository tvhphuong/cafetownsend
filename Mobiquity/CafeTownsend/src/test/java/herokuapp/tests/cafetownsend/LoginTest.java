package herokuapp.tests.cafetownsend;

import herokuapp.requirements.Application;
import herokuapp.tests.BaseTest;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Manual;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.model.TestResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.core.annotation.Order;

/**
 * Test cases for login function
 *
 *  @author Phuong Trinh Vo Hoang
 */
@Story(Application.Authentication.class)
@RunWith(SerenityRunner.class)
public class LoginTest extends BaseTest {

    @Test
    public void loginSuccessfullyWithValidAccount() {
        user.loginWithAccount("Luke", "Skywalker");
        user.verifyLoginSuccess("Luke");
    }

    @Test
    public void loginUnsuccessfullyWithInvalidUserName() {
        user.loginWithAccount("Phuong", "Skywalker");
        user.verifyTheLoginErrorMessageShouldBeShown();
    }

    @Test
    public void loginUnsuccessfullyWithInvalidPassword() {
        user.loginWithAccount("Luke", "abc");
        user.verifyTheLoginErrorMessageShouldBeShown();
    }
}