package herokuapp.tests.cafetownsend;

import herokuapp.dtos.EmployeeDTO;
import herokuapp.requirements.Application;
import herokuapp.tests.BaseTest;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Story;
import org.aspectj.lang.annotation.After;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@Story(Application.CreateAndModifyEmployee.class)
@RunWith(SerenityRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CreateAndModifyEmployeeTest extends BaseTest {

    private EmployeeDTO validEmployee =
            new EmployeeDTO("Phuong", "Trinh Vo Hoang", "2019-12-05", "test_townsend@gmail.com");

    private EmployeeDTO updateEmployee =
            new EmployeeDTO("Steve", "Jobs", "2019-07-03", "test_townsend@gmail.com");

    @Test
    public void createNewEmployeeSuccessfullyWithValidInfo() {
        user.loginWithValidUser();
        user.createEmployee(validEmployee);
        user.verifyThatEmployeeIsUpdatedInList(validEmployee);
    }

    @Test
   public void modifyEmployeeSuccessfullyWithValidInfo() {
        user.loginWithValidUser();
        user.updateEmployee(validEmployee, updateEmployee);
        user.verifyThatEmployeeIsUpdatedInList(updateEmployee);
    }

    @Test
    public void removeEmployeeSuccessfully() {
        user.loginWithValidUser();
        user.deleteEmployee(updateEmployee);
        user.verifyThatUserIsDeleted(updateEmployee);
    }

}