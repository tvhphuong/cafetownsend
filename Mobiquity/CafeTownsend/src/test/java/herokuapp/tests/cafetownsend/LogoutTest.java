package herokuapp.tests.cafetownsend;

import herokuapp.requirements.Application;
import herokuapp.tests.BaseTest;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Story;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Test cases for logout function
 *
 *  @author Phuong Trinh Vo Hoang
 */
@Story(Application.Authentication.class)
@RunWith(SerenityRunner.class)
public class LogoutTest extends BaseTest {

    @Test
    public void logoutSuccessfullyWhenUserInLogged() {
        user.loginWithValidUser();
        user.logout();
        user.verifyThatUserSeeLoginScreen();
    }
}