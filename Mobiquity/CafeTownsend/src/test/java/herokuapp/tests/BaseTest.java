package herokuapp.tests;

import herokuapp.constants.CafeTownsendConsts;
import herokuapp.steps.EmployeeManagementSteps;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.Pages;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

public class BaseTest {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = CafeTownsendConsts.DEFAULT_URL)
    public Pages pages;

    @Steps
    public EmployeeManagementSteps user;

    @Before
    public void setup() {
        user.openCafeTownSendSite();
    }

}