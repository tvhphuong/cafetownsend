package herokuapp.steps;

import herokuapp.constants.CafeTownsendConsts;
import herokuapp.dtos.EmployeeDTO;
import herokuapp.pages.EmployeesPage;
import herokuapp.pages.LoginPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Scenarios Steps testing Cafe TownSend Employee Management system
 *
 *  @author Phuong Trinh Vo Hoang
 */
public class EmployeeManagementSteps {

    LoginPage loginPage;
    EmployeesPage employeesPage;

    String username;

    @Step("Given user navigate to Cafe TownSend site")
    public void openCafeTownSendSite() {
        loginPage.open();
    }

    @Step("When user login with username '{0}' and password '{1}'")
    public void loginWithAccount(String username, String password) {
        loginPage.loginToApplication(username, password);
    }

    @Step("Given user login success with valid account")
    public void loginWithValidUser() {
        loginPage.loginToApplication(CafeTownsendConsts.VALID_USERNAME, CafeTownsendConsts.VALID_PASSWORD);
    }

    @Step("Then user should see the employee screen and correct greeting on top left")
    public void verifyLoginSuccess(String username) {
        assertThat(employeesPage.getCurrentLoginUser()).isEqualTo(username);
    }

    @Step("Then user should see the login error message")
    public void verifyTheLoginErrorMessageShouldBeShown() {
        assertThat(loginPage.loginErrorMessage.getText()).isEqualTo(CafeTownsendConsts.LOGIN_ERROR_MESSAGE);
    }

    @Step("When user create new employee with following information {0}")
    public void createEmployee(EmployeeDTO employee) {
        employeesPage.createButton.click();
        employeesPage.modifyEmployee(employee);
        employeesPage.addButton.waitUntilClickable().click();
    }

    @Step("Then user should see the employee '{0} is updated in the list")
    public void verifyThatEmployeeIsUpdatedInList(EmployeeDTO employee) {
        employeesPage.editButton.waitUntilClickable();
        assertThat(employeesPage.countExpectedEmployeeInList(employee)).isEqualTo(1);
    }

    @Step("When user update the employee with following inforfmation")
    public void updateEmployee(EmployeeDTO oldInfo, EmployeeDTO newInfo) {
        employeesPage.selectEmployee(oldInfo);
        employeesPage.editButton.click();
        employeesPage.modifyEmployee(newInfo);
        employeesPage.updateInModifyButton.waitUntilClickable().click();
    }

    @Step("When delete the employee {0}")
    public void deleteEmployee(EmployeeDTO employee) {
        employeesPage.selectEmployee(employee);
        employeesPage.clickElementAndAcceptTheAlert(employeesPage.deleteButton);

    }

    @Step("Then user should see this employee is removed out of the list")
    public void verifyThatUserIsDeleted(EmployeeDTO employee) {
        loginPage.waitForAjax(loginPage.getDriver());
        assertThat(employeesPage.waitForElementDisappear(String.format(employeesPage.employeeRow, employeesPage.getFullName(employee))))
                .isEqualTo(true);
    }

    @Step("When user do logout")
    public void logout() {
        employeesPage.createButton.waitUntilClickable();
        employeesPage.clickOnElementByOffset(employeesPage.logoutButton);
    }

    @Step("Then user should see the login screen")
    public void verifyThatUserSeeLoginScreen() {
        Assert.assertTrue(loginPage.waitForElementAppear(loginPage.loginLocator));
    }
}