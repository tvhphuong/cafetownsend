package herokuapp.pages;

import herokuapp.constants.CafeTownsendConsts;
import herokuapp.dtos.EmployeeDTO;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


/**
 * EmployeesPage
 *
 * @author Phuong Trinh Vo Hoang
 */
public class EmployeesPage extends BasePage {

    @FindBy(id = "bAdd")
    public WebElementFacade createButton;
    @FindBy(id = "bEdit")
    public WebElementFacade editButton;
    @FindBy(id = "bDelete")
    public WebElementFacade deleteButton;
    @FindBy(xpath = "//p[@class='main-button' and text()='Logout']")
    public WebElementFacade logoutButton;
    @FindBy(xpath = "//input[@ng-model='selectedEmployee.firstName']")
    public WebElementFacade firstnameTextbox;
    @FindBy(xpath = "//input[@ng-model='selectedEmployee.lastName']")
    public WebElementFacade lastnameTextbox;
    @FindBy(xpath = "//input[@ng-model='selectedEmployee.startDate']")
    public WebElementFacade startdateTextbox;
    @FindBy(xpath = "//input[@ng-model='selectedEmployee.email']")
    public WebElementFacade emailTextbox;
    @FindBy(xpath = "//button[@type='submit' and text()='Add']")
    public WebElementFacade addButton;
    @FindBy(xpath = "//button[@type='submit' and text()='Cancel']")
    public WebElementFacade cancelButton;
    @FindBy(xpath = "//button[@type='submit' and text()='Update']")
    public WebElementFacade updateInModifyButton;
    @FindBy(xpath = "//button[@type='submit' and text()='Delete']")
    public WebElementFacade deleteInModifyButton;
    @FindBy(id = "greetings")
    public WebElementFacade greetingText;

    public String employeeRow = "//li[contains(text(),'%s')]";

    public EmployeesPage(WebDriver driver) {
        super (driver);
    }

    public void modifyEmployee(EmployeeDTO info) {
    	clearEmployeeForm();
        firstnameTextbox.waitUntilVisible().sendKeys(info.getFirstName());
        lastnameTextbox.sendKeys(info.getLastName());
        startdateTextbox.sendKeys(info.getStartDate());
        emailTextbox.sendKeys(info.getEmail());
    }
    
    public void clearEmployeeForm() {
    	firstnameTextbox.clear();
    	lastnameTextbox.clear();
    	startdateTextbox.clear();
    	emailTextbox.clear();
    }

    public String getCurrentLoginUser() {
        return greetingText.waitUntilPresent().getText().replace(CafeTownsendConsts.GREETING_FORMAT, "");
    }

    public int countExpectedEmployeeInList(EmployeeDTO employee){
        return getDriver().findElements(By.xpath(String.format(employeeRow, getFullName(employee)))).size();
    }

    public void selectEmployee(EmployeeDTO employee){
        createButton.waitUntilVisible();
        element(getDriver().findElement(By.xpath(String.format(employeeRow, getFullName(employee))))).waitUntilVisible().click();
    }

    public String getFullName(EmployeeDTO employee){
        return employee.getFirstName() + " " + employee.getLastName();
    }
}