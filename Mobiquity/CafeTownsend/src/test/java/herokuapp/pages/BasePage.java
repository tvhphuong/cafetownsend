package herokuapp.pages;

import herokuapp.constants.CafeTownsendConsts;
import herokuapp.dtos.EmployeeDTO;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.webdriver.javascript.JavascriptExecutorFacade;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

/**
 * BasePage contains basic method that can reuse in other PageObject classes
 *
 * @author Phuong Trinh Vo Hoang
 */
public class BasePage extends PageObject {

    public BasePage(WebDriver driver) {
        super(driver);
    }

    public Actions builder = new Actions(getDriver());
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    public void clickElementJS(String id){
        JavascriptExecutorFacade jse = (JavascriptExecutorFacade) getDriver();
        jse.executeScript(String.format("document.getElementById('%s').click();", id));
    }

    public void clickOnElementByOffset(WebElementFacade element) {
        builder.moveByOffset(element.getLocation().x,
                element.getLocation().y).click().perform();
    }

    public void acceptAlert(){
        getDriver().switchTo().alert().accept();
    }

    public void clickElementAndAcceptTheAlert(WebElementFacade element) {
        try {
            element.waitUntilClickable().click();
        } catch( UnhandledAlertException f) {
        	//Just ignore
        }
        Alert alert = getDriver().switchTo().alert();
        alert.accept();
    }

    public boolean waitForElementDisappear(String locator){
        return waitForElementWithCondition(locator, 0);
    }

    public boolean waitForElementAppear(String locator){
        return waitForElementWithCondition(locator, 1);
    }

    public boolean waitForElementWithCondition(String locator, int count){
        for (int i = 0; i < CafeTownsendConsts.LOADING_RETRY_TIME; i++) {
            if (getDriver().findElements(By.xpath(locator)).size() != count) {
                try {
                    Thread.sleep(CafeTownsendConsts.LOADING_RETRY_POLLING);
                } catch (InterruptedException e) {
                    logger.info("Try to wait element");
                }
            } else {
                return true;
            }
        }
        return false;
    }

    public static void waitForAjax(WebDriver driver) {
        FluentWait<WebDriver> waitAjax = new FluentWait<WebDriver>(driver);
        waitAjax.until((ExpectedCondition<Boolean>) driver1 -> {
            JavascriptExecutor js = (JavascriptExecutor) driver1;
            System.out.println("STARTING WAITING: is jQuery defined - "
                    + ((JavascriptExecutor) driver1)
                    .executeScript("return window.jQuery != undefined"));
            if((Boolean) js.executeScript("return window.jQuery != undefined")){
                System.out.println("STARTING WAITING: Ajax finish - "
                        + ((JavascriptExecutor) driver1)
                        .executeScript("return window.jQuery != undefined"));
                return (Boolean) js.executeScript("return window.jQuery.active == 0");
            }
            return true;
        });
    }
}
