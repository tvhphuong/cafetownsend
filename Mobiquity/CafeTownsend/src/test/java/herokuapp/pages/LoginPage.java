package herokuapp.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;

/**
 * LoginPage
 *
 * @author Phuong Trinh Vo Hoang
 */
@DefaultUrl("/login")
public class LoginPage extends BasePage {
    @FindBy(xpath = "//input[@ng-model='user.name']")
    public WebElementFacade usernameTextBox;
    @FindBy(xpath = "//input[@ng-model='user.password']")
    public WebElementFacade passwordTextBox;
    @FindBy(xpath = "//button[@type='submit' and text()='Login']")
    public WebElementFacade loginButton;
    @FindBy(xpath = "//p[contains(@class,'error-message')]")
    public WebElementFacade loginErrorMessage;

    public String loginLocator = "//button[@type='submit' and text()='Login']";

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void loginToApplication(String username, String password) {
        usernameTextBox.sendKeys(username);
        passwordTextBox.sendKeys(password);
        loginButton.waitUntilClickable().click();
    }

    public void clearLoginForm() {
        usernameTextBox.clear();
        passwordTextBox.clear();
    }
}
