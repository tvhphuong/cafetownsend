## Cafe Townsend Automation Tests

This project contains a Serenity framework and Cafe Townsend Automation Tests

### How to setup the project
Using git to check out this source code
https://gitlab.com/tvhphuong/cafetownsend

#### Project installation

Project can be added in the IDE as a Maven project
Before starting the test, project need to be compiled

```
$ mvn clean install
```

OR if your maven repository missing any dependency, please use this command for update

```
$ mvn compile -U
```
Other maven command can be found here: https://maven.apache.org/plugins/index.html

#### Test executor
Test can be run in your IDE with JUnit test executor or can be run with maven command 
Running all the tests as follows with default webdriver Firefox (version < 71) and default gecko driver
```
$ mvn clean verify
```

Running all test with specific driver 
```
$ mvn clean verify -Dwebdriver.driver=chrome -Dwebdriver.chrome.driver=src/test/resources/weddriver/mac/chromedriver
```
Note: 
The default of webdriver.driver and webdriver.chrome.driver can be modified at ./serenity.properties
Before running the test please check if your browser version is fit with you webdriver configuration

#### Test report generator
After running all tests with maven command above, test report can be generated with
```
mvn serenity:aggregate
```
Report will be stored inside folder ./target/site/index.html